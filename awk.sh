#!/bin/bash -f
user=$1;   #assign  input variable to variable user
awk '{ FS = ":"
		if (($1 == "'$user'") || (index($4, "'$user'")))
				{
				print "GroupName : "$1 " ; GroupID : "$3;
				}
	}
	' /etc/group   
	
#/etc/group  defines the groups to which users belong under Linux
#column 1 of /etc/group contains groupname   		$1
#column 2 of /etc/group contains password			$2
#column 3 of /etc/group contains id					$3
#column 4 of /etc/group contains list of users		$4


#($1 == "'$user'")		check the specify user in column 1 (a user is automatically a group) 
#(index($4, "'$user'")	check the specify user in column 4 (the list of users)